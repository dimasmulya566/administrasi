FROM openjdk:latest
ADD target/data_warga-0.0.1-SNAPSHOT.jar /opt/app.jar
RUN bash -c 'touch /opt/app.jar'
VOLUME /usr/src/app
ARG JAR_FILE=target/*.jar
ENV DB_HOST $DB_HOST
ENV DB_PORT $DB_PORT
ENV DB_NAME $DB_NAME
ENV DB_USERNAME $DB_USERNAME
ENV DB_PASSWORD $DB_PASSWORD
ENV APP_PROFILE $APP_PROFILE
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/opt/app.jar"]
