package com.data.warga.data_warga;

import com.data.warga.data_warga.filters.AuthFilter;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
@SpringBootApplication
@OpenAPIDefinition
@EnableJpaAuditing
@EnableCaching
public class DataWargaApplication {

	public static void main(String[] args) {
		SpringApplication.run(DataWargaApplication.class, args);
	}
	@Bean
	public ModelMapper modelMapper() {
		return new ModelMapper();
	}

	@Bean
	public FilterRegistrationBean<AuthFilter> loggingFilter(){
		FilterRegistrationBean<AuthFilter> registrationBean
				= new FilterRegistrationBean<>();

		registrationBean.setFilter(new AuthFilter());
		registrationBean.addUrlPatterns("/api/*");

		return registrationBean;
	}


}
