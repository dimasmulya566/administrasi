package com.data.warga.data_warga.controller;

import com.data.warga.data_warga.components.endpoint.AlamatEndPoint;
import com.data.warga.data_warga.components.endpoint.WargaEndpoint;
import com.data.warga.data_warga.dto.DataPengurusRequest;
import com.data.warga.data_warga.dto.DataPengurusResponse;
import com.data.warga.data_warga.models.DataPengurus;
import com.data.warga.data_warga.models.Warga;
import com.data.warga.data_warga.util.ResponseData;
import com.data.warga.data_warga.service.DataPengurusService;
import net.sf.jasperreports.web.actions.SearchData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.Arrays;
import java.util.Map;

import static com.data.warga.data_warga.components.constant.DataBaseConstant.*;
import static com.data.warga.data_warga.components.endpoint.DataPengurusEndPoint.*;

@Controller
@RestController
@RequestMapping(value = path)
@CrossOrigin(origins = "http://localhost:8081")
public class DataPengurusController {

    @Autowired
    private DataPengurusService service;

    @PostMapping(value = path)
    @ResponseStatus(HttpStatus.CREATED)
    @CrossOrigin(origins = "http://localhost:8081")
    public ResponseEntity<ResponseData<Object>> createData(@Valid @RequestBody
                                                                       DataPengurusRequest request, Errors errors)
    {

        ResponseData<Object> responseData = new ResponseData<>();
        if (errors.hasErrors()){
            for(ObjectError error : errors.getAllErrors()){
                responseData.getMessages().add(error.getDefaultMessage());
            }
            responseData.setStatus(false);
            responseData.setMessageValidation(messageError);
            responseData.setPayload(null);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseData);

        }   else {
            responseData.setStatus(true);
            responseData.setMessageValidation(messageDone);
            responseData.setPayload(service.create(request));

        }
        return ResponseEntity.ok(responseData);
    }

    @GetMapping(value = path)
    @CrossOrigin(origins = "http://localhost:8081")
    public Map<String, Object> getDataPengurusResponses(){
        return service.getDataPengurusResponses();
    }

    @DeleteMapping(value = pathURL)
    @CrossOrigin(origins = "http://localhost:8081")
    public void delete (@PathVariable(pathVariableId) Long id){
        service.delete(id);
    }

    @PutMapping(value = pathURL)
    @CrossOrigin(origins = "http://localhost:8081")
    public ResponseEntity<Object> updateDataPengurus(@Valid @PathVariable(pathVariableId) Long id,
                                                  @RequestBody DataPengurusRequest request)
    {           ResponseData<Object> responseData = new ResponseData<>();
        try {
            responseData.setStatus(true);
            responseData.setPayload(service.update(id, request));
            responseData.setMessageValidation(messageUpdate);
            return ResponseEntity.ok(responseData);
        } catch (Exception ex) {
            responseData.setStatus(false);
            responseData.setMessageValidation(messageErrorUpdate);
            responseData.getMessages().add("ERROR");
            responseData.setPayload(null);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseData);
        }
    }

    @GetMapping(value = pathURL)
    public Map<String, Object> getById(@RequestParam(pathVariableId) Long id) {
        return service.getById(id);
    }

    @PostMapping("/search/{size}/{page}")
    @CrossOrigin(origins = "http://localhost:8081")
    public Map<String, Object> findByNama(@RequestBody SearchData searchData, @PathVariable(pathVariableSize) int size,
                                      @PathVariable(pathVariablePage) int page){
        Pageable pageable = PageRequest.of(page, size);

        return service.findByNama(searchData.getSearchString(), pageable);
    }

    @PostMapping("/search/{size}/{page}/{sort}")
    @CrossOrigin(origins = "http://localhost:8081")
    public Map<String, Object> findByName(@RequestBody SearchData searchData, @PathVariable("size") int size,
                                      @PathVariable("page") int page, @PathVariable("sort") String sort){
        Pageable pageable = PageRequest.of(page, size, Sort.by("id"));
        if(sort.equalsIgnoreCase("desc")){
            pageable = PageRequest.of(page, size, Sort.by("id").descending());
        }
        return service.findByNama(searchData.getSearchString(), pageable);
    }

    @PostMapping("/createAll")
    @CrossOrigin(origins = "http://localhost:8081")
    public ResponseEntity<ResponseData<Iterable<DataPengurus>>> createBatch(@RequestBody DataPengurus[] dataPenguruses){
        ResponseData<Iterable<DataPengurus>> responseData = new ResponseData<>();
        responseData.setPayload(service.saveDataPengurus(Arrays.asList(dataPenguruses)));
        responseData.setStatus(true);
        return ResponseEntity.ok(responseData);
    }

}
