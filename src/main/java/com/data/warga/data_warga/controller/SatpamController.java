package com.data.warga.data_warga.controller;

import com.data.warga.data_warga.components.endpoint.SatpamEndPoint;
import com.data.warga.data_warga.util.ResponseData;
import com.data.warga.data_warga.dto.SatpamRequest;
import com.data.warga.data_warga.dto.SatpamResponse;
import com.data.warga.data_warga.service.SatpamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = SatpamEndPoint.path)
@CrossOrigin(origins = "http://localhost:8080")
public class SatpamController {
    @Autowired
    private SatpamService service;

    @GetMapping(value = SatpamEndPoint.path)
    @CrossOrigin(origins = "http://localhost:8081")
    public List<SatpamResponse> findAll(){
        return service.findAll();
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @CrossOrigin(origins = "http://localhost:8081")
    public ResponseEntity<ResponseData<Object>> create(@Valid @RequestParam("alamat_id") Long id,
                                                       @RequestBody SatpamRequest request) {
        ResponseData<Object> responseData = new ResponseData<>();
        try {
            responseData.setStatus(true);
            responseData.setPayload(service.createSatpam(id, request));
            return ResponseEntity.ok(responseData);
        } catch (Exception ex) {
            responseData.setStatus(false);
            responseData.getMessages().add("ERROR");
            responseData.setPayload(null);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseData);
        }
    }

    @DeleteMapping(value = SatpamEndPoint.pathURL)
    @CrossOrigin(origins = "http://localhost:8081")
    public void delete (@PathVariable("id") Long id){
        service.delete(id);
    }

    @PutMapping(value = SatpamEndPoint.pathURL)
    @CrossOrigin(origins = "http://localhost:8081")
    public ResponseEntity<Object> updateDataAlamat(@Valid @PathVariable("id") Long id,
                                                   @RequestBody SatpamRequest request) {
        ResponseData<Object> responseData = new ResponseData<>();
        try {
            responseData.setStatus(true);
            responseData.setPayload(service.updateSatpam(id, request));
            return ResponseEntity.ok(responseData);
        } catch (Exception ex) {
            responseData.setStatus(false);
            responseData.getMessages().add("ERROR");
            responseData.setPayload(null);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseData);
        }
    }

    @GetMapping(value = SatpamEndPoint.pathURL)
    @CrossOrigin(origins = "http://localhost:8081")
    public Map<String, Object> getById(@RequestParam("id") Long id) {
        return service.getById(id);
    }
}
