package com.data.warga.data_warga.controller;

import com.data.warga.data_warga.components.endpoint.AlamatEndPoint;
import com.data.warga.data_warga.dto.AlamatRequest;
import com.data.warga.data_warga.dto.AlamatResponse;
import com.data.warga.data_warga.dto.WargaResponse;
import com.data.warga.data_warga.util.ResponseData;
import com.data.warga.data_warga.service.AlamatService;
import com.data.warga.data_warga.util.ErrorParseUtility;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

import static com.data.warga.data_warga.components.constant.DataBaseConstant.*;

@RestController
@RequestMapping("/alamat")
@CrossOrigin(origins = "http://localhost:8081")
public class AlamatController {

    @Autowired
    private AlamatService service;

    @GetMapping(value = AlamatEndPoint.path)
    @CrossOrigin(origins = "http://localhost:8081")
    public List<AlamatResponse> findAll(){
        return service.findAll();
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @CrossOrigin(origins = "http://localhost:8081")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK", content = {@Content
                    (mediaType = "application/json",
                            schema = @Schema(implementation = AlamatResponse.class))})
    })
    public ResponseEntity<ResponseData<Object>> createData(@Valid @RequestParam("province_id") Integer province_id,@RequestParam("city_id") Integer city_id,
                                                           @RequestParam("kel_id") Integer kel_id,@RequestParam("zipcode") Integer zipcode,
                                                           @RequestParam("kec_id") Integer kec_id,@RequestParam("id") Long id,
                                                           @RequestBody AlamatRequest request,Errors errors)
    {
        ResponseData<Object> responseData = new ResponseData<>();
        if (errors.hasErrors()){
            responseData.setStatus(false);
            responseData.setMessages(ErrorParseUtility.parse(errors));
            }
        try {
            responseData.setStatus(true);
            responseData.setMessageValidation(messageDone);
            responseData.setPayload(service.create(province_id,city_id,kec_id,kel_id,zipcode,id, request));
            return ResponseEntity.ok(responseData);
        } catch (Exception ex) {
            responseData.setStatus(false);
            responseData.getMessages().add(ex.getMessage());
            responseData.setMessageValidation(messageError);
            responseData.setPayload(null);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseData);
        }
    }

    @DeleteMapping(value = AlamatEndPoint.pathURL)
    @CrossOrigin(origins = "http://localhost:8081")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK", content = {@Content
                    (mediaType = "application/json",
                            schema = @Schema(implementation = AlamatResponse.class))})
    })
    public void delete (@PathVariable("id") Long id){
        service.delete(id);
    }

    @PutMapping(value = AlamatEndPoint.pathURL)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK", content = {@Content
                    (mediaType = "application/json",
                            schema = @Schema(implementation = AlamatResponse.class))})
    })
    public ResponseEntity<Object> updateDataAlamat(@Valid  @PathVariable("province_id") Integer province_id,@PathVariable("city_id") Integer city_id,
                                                   @PathVariable("kel_id") Integer kel_id,@PathVariable("zipcode") Integer zipcode,
                                                   @PathVariable("kec_id") Integer kec_id,@PathVariable("id") Long id,
                                                   @RequestBody AlamatRequest request) {
        ResponseData<Object> responseData = new ResponseData<>();
        try {
            responseData.setStatus(true);
            responseData.setMessageValidation(messageUpdate);
            responseData.setPayload(service.update(province_id, city_id, kel_id, zipcode, kec_id, id, request));
            return ResponseEntity.ok(responseData);
        } catch (Exception ex) {
            responseData.setStatus(false);
            responseData.setMessageValidation(messageErrorUpdate);
            responseData.getMessages();
            responseData.setPayload(null);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseData);
        }
    }

    @GetMapping(value = AlamatEndPoint.pathURL)
    @CrossOrigin(origins = "http://localhost:8081")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK", content = {@Content
                    (mediaType = "application/json",
                            schema = @Schema(implementation = AlamatResponse.class))})
    })
    public Map<String, Object> getById(@RequestParam("id") Long id) {
        return service.getById(id);
    }

}
