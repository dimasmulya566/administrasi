package com.data.warga.data_warga.controller;

import com.data.warga.data_warga.components.endpoint.WargaEndpoint;
import com.data.warga.data_warga.util.ResponseData;
import com.data.warga.data_warga.dto.WargaRequest;
import com.data.warga.data_warga.dto.WargaResponse;
import com.data.warga.data_warga.models.Warga;
import com.data.warga.data_warga.service.WargaService;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import net.sf.jasperreports.web.actions.SearchData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import static com.data.warga.data_warga.components.endpoint.WargaEndpoint.*;
import static com.data.warga.data_warga.components.constant.DataBaseConstant.*;

@RestController
@RequestMapping(value = "/api")
@CrossOrigin(origins = "http://localhost:8081")
public class WargaController {

    @Autowired
    private WargaService service;


    @PostMapping(path = WargaEndpoint.path)
    @CrossOrigin(origins = "http://localhost:8081")
    @ResponseStatus(HttpStatus.CREATED)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK", content = {@Content
                    (mediaType = "application/json",
                            schema = @Schema(implementation = WargaResponse.class))})
    })
    public ResponseEntity<ResponseData<Object>> createData(HttpServletRequest httpServletRequest,@Valid @RequestBody
                                                                       WargaRequest request, Errors errors)
    {
        ResponseData<Object> responseData = new ResponseData<>();
        int page = (Integer) httpServletRequest.getAttribute("id");
        if (errors.hasErrors()){
            for(ObjectError error : errors.getAllErrors()){
                responseData.getMessages().add(error.getDefaultMessage());
            }
            responseData.setStatus(false);
            responseData.setMessageValidation(messageError);
            responseData.setPayload(null);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseData);

        }   else {
            responseData.setStatus(true);
            responseData.setMessageValidation(messageDone);
            responseData.setPayload(service.create(page,request));

        }
        return ResponseEntity.ok(responseData);
    }

    @GetMapping(path = WargaEndpoint.path)
    @CrossOrigin(origins = "http://localhost:8081")
    @ResponseStatus(HttpStatus.OK)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK", content = {@Content
                    (mediaType = "application/json",
                            schema = @Schema(implementation = WargaResponse.class))})
    })
    public ResponseEntity<List<Warga>> findAll(HttpServletRequest request){
//        return service.findAll(jwtTokenUtils.getUsernameFromToken(request.getHeader("Authorization")));
        int page = (Integer) request.getAttribute("id");
        List<Warga> findall= service.findAll(page);
        return new ResponseEntity<>(findall,HttpStatus.OK);

    }

    @DeleteMapping(path = WargaEndpoint.pathURL)
    @CrossOrigin(origins = "http://localhost:8081")
    @ResponseStatus(HttpStatus.OK)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK", content = {@Content
                    (mediaType = "application/json",
                            schema = @Schema(implementation = WargaResponse.class))})
    })
    public ResponseEntity<?> delete(@RequestParam(pathVariableId) Long id){
        Map<String, Object> delete = service.delete(id);
        return ResponseEntity.ok(delete);
    }


    @PutMapping(path = WargaEndpoint.pathURL)
    @CrossOrigin(origins = "http://localhost:8081")
    @ResponseStatus(HttpStatus.CREATED)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK", content = {@Content
                    (mediaType = "application/json",
                            schema = @Schema(implementation = WargaResponse.class))})
    })
    public ResponseEntity<Object> updateDataWarga(HttpServletRequest httpServletRequest,@Valid @PathVariable(pathVariableId) Long id,
                                                                @RequestBody WargaRequest request)
    {           ResponseData<Object> responseData = new ResponseData<>();
        int page = (Integer) httpServletRequest.getAttribute("id");
        try {
            responseData.setStatus(true);
            responseData.setMessageValidation(messageUpdate);
            responseData.setPayload(service.update(page,id, request));
            return ResponseEntity.ok(responseData);
        } catch (Exception ex) {
            responseData.setStatus(false);
            responseData.setMessageValidation(messageErrorUpdate);
            responseData.getMessages().add("ERROR");
            responseData.setPayload(null);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseData);
        }
    }

    @GetMapping(path = WargaEndpoint.pathURL)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK", content = {@Content
                    (mediaType = "application/json",
                            schema = @Schema(implementation = WargaResponse.class))})
    })
    public Map<String, Object> getById(@RequestParam(pathVariableId) Long id) {
        return service.getById(id);
    }

    @PostMapping("/search/{size}/{page}")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK", content = {@Content
                    (mediaType = "application/json",
                            schema = @Schema(implementation = WargaResponse.class))})
    })
    public Iterable<Warga> findByName(@RequestBody SearchData searchData, @PathVariable(pathVariableSize) int size,
                                      @PathVariable(pathVariablePage) int page){
        Pageable pageable = PageRequest.of(page, size);

        return service.findByNama(searchData.getSearchString(), pageable);
    }

    @PostMapping("/search/{size}/{page}/{sort}")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK", content = {@Content
                    (mediaType = "application/json",
                            schema = @Schema(implementation = WargaResponse.class))})
    })
    public Iterable<Warga> findByName(@RequestBody SearchData searchData, @PathVariable("size") int size,
                                      @PathVariable("page") int page, @PathVariable("sort") String sort){
        Pageable pageable = PageRequest.of(page, size, Sort.by("id"));
        if(sort.equalsIgnoreCase("desc")){
            pageable = PageRequest.of(page, size, Sort.by("id").descending());
        }
        return service.findByNama(searchData.getSearchString(), pageable);
    }

    @PostMapping("/createAll")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK", content = {@Content
                    (mediaType = "application/json",
                            schema = @Schema(implementation = WargaResponse.class))})
    })
    public ResponseEntity<ResponseData<Iterable<Warga>>> createBatch(@RequestBody Warga[] wargas){
        ResponseData<Iterable<Warga>> responseData = new ResponseData<>();
        responseData.setPayload(service.saveWarga(Arrays.asList(wargas)));
        responseData.setStatus(false);
        return ResponseEntity.ok(responseData);
    }
}