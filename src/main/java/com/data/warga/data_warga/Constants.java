package com.data.warga.data_warga;

public class Constants {

    public static final String API_SECRET_KEY = "data_wargaSecretKey";
    public static final long TOKEN_VALIDITY = 2 * 60 * 60 * 1000;
}
