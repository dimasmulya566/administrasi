package com.data.warga.data_warga.components.endpoint;

public final class SatpamEndPoint {
    public static final String pathVariableId = "id";
    public static final String path = "/tampilkan/dataSatpamWarga";
    public static final String pathURL = "/{" + pathVariableId + "}";
}
