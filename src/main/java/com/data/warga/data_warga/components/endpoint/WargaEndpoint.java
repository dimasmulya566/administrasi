package com.data.warga.data_warga.components.endpoint;

public final class WargaEndpoint {
    public static final String pathVariableId = "id";
    public static final String path = "/auth/tampilkan/dataWarga";
    public static final String pathURL = "/{" + pathVariableId + "}";
    public static final String pathVariableSize = "size";
    public static final String pathVariablePage = "page";
}
