package com.data.warga.data_warga.components.constant;

public class WargaConstant {
    public static final String emptyNoKartuKeluarga = "No Kartu Keluarga tidak boleh kosong";
    public static final String emptyNama = "Nama tidak boleh kosong";
    public static final String emptyTempatTanggalLahir = "Tempat, Tanggal lahir tidak boleh kosong";
    public static final String emptyAgama = "Agama tidak boleh kosong";
    public static final String emptyJenisKelamin = "Jenis Kelamin tidak boleh kosong";

    public static final String nullNoKartuKeluarga = "No Kartu Keluarga harus di isi";
    public static final String nullNama = "Nama harus di isi";
    public static final String nullTempatTanggalLahir = "Tempat, Tanggal lahir harus di isi";
    public static final String nullAgama = "Agama harus di isi";
    public static final String nullJenisKelamin = "Jenis Kelamin harus di isi";

}
