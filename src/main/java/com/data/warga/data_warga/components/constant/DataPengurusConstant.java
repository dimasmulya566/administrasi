package com.data.warga.data_warga.components.constant;

public class DataPengurusConstant {
    public static final String emptyUmur = "Umur tidak boleh kosong";
    public static final String emptyMassaKerja = "Massa Kerja tidak boleh kosong";
}
