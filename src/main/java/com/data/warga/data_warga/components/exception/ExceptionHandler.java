package com.data.warga.data_warga.components.exception;

public class ExceptionHandler extends Exception{

    public ExceptionHandler() {
        super();
    }

    public ExceptionHandler(String message) {
        super(message);
    }
}
