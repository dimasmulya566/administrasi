package com.data.warga.data_warga.components.endpoint;

public final class IuranWargaEndPoint {
    public static final String pathVariableId = "id";
    public static final String path = "/tampilkan/dataIuranWarga";
    public static final String pathURL = "/{" + pathVariableId + "}";
}
