package com.data.warga.data_warga.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class IuranWargaResponse {
    @JsonIgnore
    private Long id;
    private String namaWarga;
    private BigDecimal iuranSampah;
    private BigDecimal iuranKebersihan;
    private BigDecimal iuranKeamanan;
    private BigDecimal iuranKasRT;
    private BigDecimal iuranKasRW;
}
