package com.data.warga.data_warga.dto;

import com.data.warga.data_warga.Enum.Shift;
import lombok.Data;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Data
public class SatpamRequest {

    private String nama;
    private Integer noTelepon;
    @Enumerated(EnumType.STRING)
    private Shift shift;

}
