package com.data.warga.data_warga.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PekerjaanRequest {

    private String pekerjaan;
    private Double penghasilan;
}
