package com.data.warga.data_warga.dto;

import lombok.Data;

@Data
public class UserRequest {
    private String username;

    private String first_name;

    private String last_name;

    private String email;

    private String password;
}
