package com.data.warga.data_warga.dto;

import com.data.warga.data_warga.models.Warga;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PekerjaanResponse {

    private Long id;
    private Warga warga;
    private String pekerjaan;
    private Double penghasilan;
}
