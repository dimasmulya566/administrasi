package com.data.warga.data_warga.dto;

import com.data.warga.data_warga.models.Warga;
import com.data.warga.data_warga.models.area.Kecamatan;
import com.data.warga.data_warga.models.area.Kelurahan;
import com.data.warga.data_warga.models.area.Kota;
import com.data.warga.data_warga.models.area.Zipcode;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import static com.data.warga.data_warga.components.constant.AlamatConstant.*;

@Data
@NoArgsConstructor
public class AlamatRequest {

    private String alamat;
}
