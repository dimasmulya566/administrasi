package com.data.warga.data_warga.dto;

import com.data.warga.data_warga.Enum.DataPengurusEnum;
import com.data.warga.data_warga.Enum.JurusanPendidikan;
import com.data.warga.data_warga.Enum.Pendidikan;
import com.data.warga.data_warga.Enum.Status;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DataPengurusResponse {

    private Long id;
    private String nama;
    @Enumerated(EnumType.STRING)
    private DataPengurusEnum jabatan;
    private String umur;
    @Enumerated(EnumType.STRING)
    private Pendidikan pendidikan;
    @Enumerated(EnumType.STRING)
    private JurusanPendidikan jurusan;
    @DateTimeFormat(pattern = "dd-MM-YYYY")
    private String massaKerja;
    @Enumerated(EnumType.STRING)
    private Status status;
}
