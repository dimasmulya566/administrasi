package com.data.warga.data_warga.service;

import com.data.warga.data_warga.components.Constant;
import com.data.warga.data_warga.dao.IuranWargaDao;
import com.data.warga.data_warga.dao.WargaDao;
import com.data.warga.data_warga.dto.DataPengurusResponse;
import com.data.warga.data_warga.dto.IuranWargaRequest;
import com.data.warga.data_warga.dto.IuranWargaResponse;
import com.data.warga.data_warga.helper.SimulateProcess;
import com.data.warga.data_warga.models.DataPengurus;
import com.data.warga.data_warga.models.IuranWarga;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@CacheConfig(cacheNames = "iuranWargaCache")
public class IuranWargaServiceImpl implements IuranWargaService {
    @Autowired
    WargaDao wargaDao;
    @Autowired
    IuranWargaDao iuranWargaDao;

    @Autowired
    private ModelMapper mapper;

    @Autowired
    private SimulateProcess simulatedProsess;

    private IuranWarga convertToEntity(IuranWargaRequest request){
        return mapper.map(request, IuranWarga.class);
    }

    private IuranWargaResponse convertToResponse(IuranWarga iuranWarga){
        return mapper.map(iuranWarga, IuranWargaResponse.class);
    }

    @Transactional(isolation = Isolation.READ_UNCOMMITTED)
    @Cacheable(cacheNames = "iuranWargaCache")
    @Override
    public Map<String, Object> getIuranWargaResponses(){
        Map<String, Object> result = new HashMap<>();
        List<IuranWargaResponse> responses = new ArrayList<>();
        simulatedProsess.simulatedProsess();
        for(IuranWarga iuranWarga : iuranWargaDao.findAll()){
            IuranWargaResponse iuranWargaResponse = convertToResponse(iuranWarga);
            responses.add(iuranWargaResponse);
        }

        String message;

        if (responses.isEmpty()){
            message = "Data Kosong";
            result.put(Constant.MESSAGE_STRING, message);
            result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
        } else {
            message = "Tampilkan Data";
            result.put(Constant.MESSAGE_STRING, message);
            result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
            result.put(Constant.DATA_STRING, responses);
            result.put(Constant.TOTAL_STRING,responses.size());
        }
        return result;
    }

    @CacheEvict(cacheNames = "iuranWargaCache", allEntries = true)
    @Override
    public Map<String, Object> createIuran(Long wargaId, IuranWargaRequest request) {
        Map<String, Object> result = new HashMap<>();
        simulatedProsess.simulatedProsess();
        if (wargaDao.existsById(wargaId)) {
            var getData = wargaDao.findById(wargaId);
            var setIuranWarga = convertToEntity(request);
            setIuranWarga.setDeleted(Boolean.FALSE);
            setIuranWarga.setWarga(getData.get());
            setIuranWarga.setIuranKasRT(request.getIuranKasRT());
            setIuranWarga.setIuranKasRW(request.getIuranKasRW());
            setIuranWarga.setIuranKeamanan(request.getIuranKeamanan());
            setIuranWarga.setIuranKebersihan(request.getIuranKebersihan());
            setIuranWarga.setIuranSampah(request.getIuranSampah());
            var resultInput = iuranWargaDao.save(setIuranWarga);

            result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
            result.put(Constant.MESSAGE_STRING, Constant.SUCCESS_STRING);
            result.put(Constant.DATA_STRING, convertToResponse(resultInput));
            return result;
        }
        result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
        result.put(Constant.MESSAGE_STRING, Constant.ID_NOT_FOUND);
        return result;

    }

    @Caching(evict = { @CacheEvict(cacheNames = "iuranWarga", key = "#id"),
            @CacheEvict(cacheNames = "iuranWargaCache", allEntries = true) })
    @Override
    public void delete(Long id) {
        simulatedProsess.simulatedProsess();
        var deleteIuran = iuranWargaDao.findById(id).stream().filter(iuranWarga ->
                iuranWarga.getDeleted().equals(Boolean.FALSE)).findFirst();
        if (deleteIuran.isPresent()){
            deleteIuran.get().setDeleted(Boolean.FALSE);
            iuranWargaDao.save(deleteIuran.get());
        }
        deleteIuran.orElseThrow();

    }

    @Transactional(propagation = Propagation.REQUIRED)
    @Cacheable(cacheNames = "iuranWargaCache", key = "#id", unless = "#result == null")
    @Override
    public Map<String, Object> getById(Long id) {
        simulatedProsess.simulatedProsess();
        Map<String, Object> result = new HashMap<>();
        try {
            var dataIuran = iuranWargaDao.findById(id);
            result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
            result.put(Constant.MESSAGE_STRING, Constant.SUCCESS_STRING);
            result.put(Constant.DATA_STRING, convertToResponse(dataIuran.get()));
        } catch (Exception e) {
            result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
            result.put(Constant.MESSAGE_STRING, Constant.ID_NOT_FOUND);
        }
        return result;
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @CacheEvict(cacheNames = "iuranWargaCache", key = "#id", allEntries = true)
    @Override
    public Map<String, Object> updateBayaran(Long id, IuranWargaRequest request) {
        Map<String, Object> result = new HashMap<>();
        simulatedProsess.simulatedProsess();
        try {
            var data = iuranWargaDao.findById(id);
            data.get().setIuranSampah(request.getIuranSampah());
            data.get().setIuranKebersihan(request.getIuranKebersihan());
            data.get().setIuranKeamanan(request.getIuranKeamanan());
            data.get().setIuranKasRT(request.getIuranKasRT());
            data.get().setIuranKasRW(request.getIuranKasRW());

            IuranWarga resultInput = iuranWargaDao.save(data.get());

            result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
            result.put(Constant.MESSAGE_STRING, Constant.SUCCESS_STRING);
            result.put(Constant.DATA_STRING, convertToResponse(resultInput));
        } catch (Exception e) {
            result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
            result.put(Constant.MESSAGE_STRING, Constant.ID_NOT_FOUND);
        }
        return result;
    }
}
