package com.data.warga.data_warga.service;

import com.data.warga.data_warga.dto.SatpamRequest;
import com.data.warga.data_warga.dto.SatpamResponse;

import java.util.List;
import java.util.Map;

public interface SatpamService {
    List<SatpamResponse> findAll();
    Map<String, Object> createSatpam(Long alamatId, SatpamRequest satpamRequest);
    void delete(Long id);
    Map<String, Object> getById(Long id);
    Map<String, Object> updateSatpam(Long id, SatpamRequest satpamRequest);
}
