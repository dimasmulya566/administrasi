package com.data.warga.data_warga.service;

import com.data.warga.data_warga.components.Constant;
import com.data.warga.data_warga.dao.AlamatDao;
import com.data.warga.data_warga.dao.PekerjaanDao;
import com.data.warga.data_warga.dao.WargaDao;
import com.data.warga.data_warga.dto.PekerjaanRequest;
import com.data.warga.data_warga.dto.PekerjaanResponse;
import com.data.warga.data_warga.helper.SimulateProcess;
import com.data.warga.data_warga.models.Pekerjaan;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@Slf4j
@CacheConfig(cacheNames = "pekerjaanCache")
public class PekerjaanServiceImpl implements PekerjaanService {

    @Autowired
    PekerjaanDao pekerjaanDao;

    @Autowired
    WargaDao wargaDao;

    @Autowired
    AlamatDao alamatDao;

    @Autowired
    private ModelMapper mapper;

    @Autowired
    private SimulateProcess simulatedProsess;


    public Pekerjaan convertToEntity(PekerjaanRequest request) {
        return mapper.map(request, Pekerjaan.class);
    }

    public PekerjaanResponse convertToResponse(Pekerjaan pekerjaan) {
        return mapper.map(pekerjaan, PekerjaanResponse.class);
    }

    @Transactional(isolation = Isolation.READ_UNCOMMITTED)
    @Cacheable(cacheNames = "pekerjaanCache")
    @Override
    public List<PekerjaanResponse> findAll() {
        simulatedProsess.simulatedProsess();
        var pekerjaanWarga = pekerjaanDao.findAll().stream().filter(pekerjaan -> pekerjaan.getDeleted()
                .equals(Boolean.FALSE)).collect(Collectors.toList());
        return pekerjaanWarga.stream().map(this :: convertToResponse).collect(Collectors.toList());
    }

    @CacheEvict(cacheNames = "pekerjaanCache", allEntries = true)
    @Override
    public Map<String, Object> create(Long wargaId, PekerjaanRequest request) {
        Map<String, Object> result = new HashMap<>();
        simulatedProsess.simulatedProsess();
        if (wargaDao.existsById(wargaId)) {
            var getData = wargaDao.findById(wargaId);
            var setPekerjaanWarga = convertToEntity(request);
            setPekerjaanWarga.setDeleted(Boolean.FALSE);
            setPekerjaanWarga.setWarga(getData.get());
            setPekerjaanWarga.setPekerjaan(request.getPekerjaan());
            setPekerjaanWarga.setPenghasilan(request.getPenghasilan());
            var resultInput = pekerjaanDao.save(setPekerjaanWarga);

            result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
            result.put(Constant.MESSAGE_STRING, Constant.SUCCESS_STRING);
            result.put(Constant.DATA_STRING, convertToResponse(resultInput));
            return result;
        }
        result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
        result.put(Constant.MESSAGE_STRING, Constant.ID_NOT_FOUND);
        return result;
    }


    @Caching(evict = { @CacheEvict(cacheNames = "pekerjaan", key = "#id"),
            @CacheEvict(cacheNames = "pekerjaanCache", allEntries = true) })
    @Override
    public void delete(Long id) {
        simulatedProsess.simulatedProsess();
        var delete = pekerjaanDao.findById(id).stream().filter(pekerjaan ->
                pekerjaan.getDeleted().equals(Boolean.FALSE)).findFirst();
        if(delete.isPresent()){
            delete.get().setDeleted(Boolean.TRUE);
            pekerjaanDao.save(delete.get());
        }
        delete.orElseThrow();
    }

    @Transactional(propagation = Propagation.REQUIRED)
    @Cacheable(cacheNames = "pekerjaanCache", key = "#id", unless = "#result == null")
    @Override
    public Map<String, Object> getById(Long id) {
        Map<String, Object> result = new HashMap<>();
        simulatedProsess.simulatedProsess();
        try {
            var data = pekerjaanDao.findById(id);
            result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
            result.put(Constant.MESSAGE_STRING, Constant.SUCCESS_STRING);
            result.put(Constant.DATA_STRING, convertToResponse(data.get()));
        } catch (Exception e) {
            result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
            result.put(Constant.MESSAGE_STRING, Constant.ID_NOT_FOUND);
        }
        return result;
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @CacheEvict(cacheNames = "pekerjaanCache", key = "#id", allEntries = true)
    @Override
    public Map<String, Object> update(Long id, PekerjaanRequest request) {
        Map<String, Object> result = new HashMap<>();
        simulatedProsess.simulatedProsess();
        try {
            var data = pekerjaanDao.findById(id);
            data.get().setPekerjaan(request.getPekerjaan());
            data.get().setPenghasilan(request.getPenghasilan());

            Pekerjaan resultInput = pekerjaanDao.save(data.get());

            result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
            result.put(Constant.MESSAGE_STRING, Constant.SUCCESS_STRING);
            result.put(Constant.DATA_STRING, convertToResponse(resultInput));
        } catch (Exception e) {
            result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
            result.put(Constant.MESSAGE_STRING, Constant.ID_NOT_FOUND);
        }
        return result;
    }
}
