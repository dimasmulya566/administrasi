package com.data.warga.data_warga.service;

import com.data.warga.data_warga.components.Constant;
import com.data.warga.data_warga.dao.WargaDao;
import com.data.warga.data_warga.dto.WargaRequest;
import com.data.warga.data_warga.dto.WargaResponse;
import com.data.warga.data_warga.helper.SimulateProcess;
import com.data.warga.data_warga.helper.WargaHelper;
import com.data.warga.data_warga.models.Warga;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
@Slf4j
@CacheConfig(cacheNames = "wargaCache")
public class WargaServiceImpl implements WargaService {

    @Autowired
    private WargaDao wargaDao;

    @Autowired
    private WargaHelper wargaHelper;

    @Autowired
    private SimulateProcess simulatedProsess;

    @Autowired
    private ModelMapper mapper;

    public WargaResponse convertToResponse(Warga warga){
        return mapper.map(warga, WargaResponse.class);
    }


    @CacheEvict(cacheNames = "wargaCache", allEntries = true)
    @Override
    public Map<String, Object> create(Integer userCode,WargaRequest wargaRequest){
        simulatedProsess.simulatedProsess();
        return wargaHelper.getStringObjectMap(wargaRequest);
    }

    @Transactional(isolation = Isolation.READ_UNCOMMITTED)
    @Cacheable(cacheNames = "wargaCache")
    @Override
    public List<Warga> findAll(Integer userCode) {
        return wargaDao.findAll();

    }


    @Caching(evict = { @CacheEvict(cacheNames = "warga", key = "#id"),
            @CacheEvict(cacheNames = "wargaCache", allEntries = true) })
    @Override
    public Map<String, Object> delete(Long id){
        Map<String, Object> result = new HashMap<>();
        simulatedProsess.simulatedProsess();
        try {
            Warga data = wargaDao.findByIdTrue(id);
            data.setDeleted(true);

            Warga resultInput = wargaDao.save(data);
            result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
            result.put(Constant.MESSAGE_STRING, Constant.SUCCESS_STRING);
            result.put(Constant.DATA_STRING, resultInput);
        }
        catch (Exception e) {
            result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
            result.put(Constant.MESSAGE_STRING, Constant.ID_NOT_FOUND);
        }
        return result;

    }

    @CacheEvict(cacheNames = "wargaCache", key = "#id", allEntries = true)
    @Override
    public WargaResponse update(Integer userCode,Long id, WargaRequest request) {
        simulatedProsess.simulatedProsess();
        var getRequest = wargaDao.findById(id);
        if (getRequest.isPresent()) {
            getRequest.get().setNama(request.getNama());
            getRequest.get().setNoKartuKeluarga(request.getNoKartuKeluarga());
            getRequest.get().setIdCard(request.getIdCard());
            getRequest.get().setNoTelepon(request.getNoTelepon());
            getRequest.get().setTempatTanggalLahir(request.getTempatTanggalLahir());
            getRequest.get().setAgama(request.getAgama());
            getRequest.get().setHubungan(request.getHubungan());
            getRequest.get().setJenisKelamin(request.getJenisKelamin());
            getRequest.get().setDeleted(Boolean.FALSE);

            var update = wargaDao.save(getRequest.get());

            return convertToResponse(update);

        }
        return convertToResponse(getRequest.orElseThrow());

    }
    @Transactional(propagation = Propagation.REQUIRED)
    @Cacheable(cacheNames = "wargaCache", key = "#id", unless = "#result == null")
    @Override
    public Map<String, Object> getById(Long id) {
        Map<String, Object> result = new HashMap<>();
        simulatedProsess.simulatedProsess();
        try {
            var data = wargaDao.findById(id);
            result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
            result.put(Constant.MESSAGE_STRING, Constant.SUCCESS_STRING);
            result.put(Constant.DATA_STRING, convertToResponse(data.get()));
        } catch (Exception e) {
            result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
            result.put(Constant.MESSAGE_STRING, Constant.ID_NOT_FOUND);
        }
        return result;
    }

    @Override
    public Warga create(Warga user) {
        simulatedProsess.simulatedProsess();
        Warga userSave = wargaDao.save(user);
        return userSave;
    }

    public Iterable<Warga> findByNama(String nama, Pageable pageable){
        simulatedProsess.simulatedProsess();
        return wargaDao.findByNamaContains(nama, pageable);
    }

    public Iterable<Warga> saveWarga(Iterable<Warga> warga){
        simulatedProsess.simulatedProsess();
        return wargaDao.saveAll(warga);
    }
}
