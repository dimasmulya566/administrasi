package com.data.warga.data_warga.service;

import com.data.warga.data_warga.dto.WargaRequest;
import com.data.warga.data_warga.dto.WargaResponse;
import com.data.warga.data_warga.models.Warga;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

public interface WargaService {
    public Map<String, Object> create(Integer userCode,WargaRequest wargaRequest);
    List<Warga>  findAll(Integer userCode);
    Map<String, Object> delete(Long id);
    public WargaResponse update(Integer userCode,Long id, WargaRequest request);
    public Map<String, Object> getById(Long id);
    Warga create(Warga userSave);
    Iterable<Warga> findByNama(String nama, Pageable pageable);
    Iterable<Warga> saveWarga(Iterable<Warga> warga);
}
