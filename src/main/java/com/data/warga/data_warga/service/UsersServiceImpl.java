package com.data.warga.data_warga.service;

import at.favre.lib.crypto.bcrypt.BCrypt;
import com.data.warga.data_warga.components.exception.EtAuthException;
import com.data.warga.data_warga.dao.UserDao;
import com.data.warga.data_warga.dto.*;
import com.data.warga.data_warga.models.Users;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.regex.Pattern;

@Service
@Transactional
public class UsersServiceImpl implements UsersService {

    @Autowired
    private UserDao userDao;
    @Autowired
    private ModelMapper mapper;
    @Autowired
    private JdbcTemplate jdbcTemplate;

    private static final String FIND_BY_EMAIL = "SELECT id,username,first_name,last_name,password,email FROM users WHERE email = ?";
    public Users convertToEntity(UserRequest request){
        return mapper.map(request, Users.class);
    }

    private RowMapper<Users> userRowMapper = ((rs, rowNum) -> {
        return new Users(
                rs.getInt("id"),
                rs.getString("username"),
                rs.getString("first_name"),
                rs.getString("last_name"),
                rs.getString("email"),
                rs.getString("password")
        );
    });

    @Override
    public Users validateUser(String email, String password) throws EtAuthException {
            Users user = jdbcTemplate.queryForObject(FIND_BY_EMAIL, new Object[]{email}, userRowMapper);
            BCrypt.Result result = BCrypt.verifyer().verify(password.toCharArray(), user.getPassword());
        char[] bcryptChars = BCrypt.with(BCrypt.Version.VERSION_2A).hashToChar(6, password.toCharArray());
        String bcryptString = new String(bcryptChars);
        if (result.verified)
            if (user.getPassword().equals(bcryptString))
                    throw new EtAuthException("Invalid password");
            return user;

    }

    @Override
    public Users register(UserRequest reqest) throws EtAuthException {
        String hashPassword = org.mindrot.jbcrypt.BCrypt.hashpw(reqest.getPassword(), org.mindrot.jbcrypt.BCrypt.gensalt(10));
        Pattern pattern = Pattern.compile("^(.+)@(.+)$");
        var email = reqest.getEmail();
        var username = reqest.getUsername();
        if (reqest.getEmail() != null) email = reqest.getEmail().toLowerCase();
        if(!pattern.matcher(email).matches())
            throw new EtAuthException("Invalid email format");
        if(!username.matches("^[a-zA-Z0-9]*$"))
            throw new EtAuthException("Invalid username format");
        if (!username.equals(reqest.getUsername())) throw new EtAuthException("Invalid username");
        Integer count = userDao.getCountEmail(email);
        if(count > 0)
            throw new EtAuthException("Email already in use");
        Integer countUsername = userDao.getCountUsername(username);
        if(countUsername > 0)
            throw new EtAuthException("Username already in use");
        var data = convertToEntity(reqest);
        data.setPassword(hashPassword);
        Users userId = userDao.save(data);
        return userId;
    }
}
