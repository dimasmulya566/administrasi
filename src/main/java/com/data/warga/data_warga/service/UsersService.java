package com.data.warga.data_warga.service;

import com.data.warga.data_warga.components.exception.EtAuthException;
import com.data.warga.data_warga.dto.UserRequest;
import com.data.warga.data_warga.models.Users;

public interface UsersService {

    Users validateUser(String email, String password) throws EtAuthException;
    Users register(UserRequest reqest) throws EtAuthException;
//    Users register(String email,String password, String userName,String firstName, String lastName) throws EtAuthException;
}
