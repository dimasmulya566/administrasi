package com.data.warga.data_warga.service;

import com.data.warga.data_warga.components.Constant;
import com.data.warga.data_warga.dao.AlamatDao;
import com.data.warga.data_warga.dao.SatpamDao;
import com.data.warga.data_warga.dto.SatpamRequest;
import com.data.warga.data_warga.dto.SatpamResponse;
import com.data.warga.data_warga.helper.SimulateProcess;
import com.data.warga.data_warga.models.Satpam;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@Slf4j
@CacheConfig(cacheNames = "satpamCache")
public class SatpamServiceImpl implements SatpamService{

    @Autowired
    SatpamDao satpamDao;
    @Autowired
    AlamatDao alamatDao;
    @Autowired
    private ModelMapper mapper;
    @Autowired
    private SimulateProcess simulatedProsess;


    private Satpam convertToEntity(SatpamRequest request){
        return mapper.map(request, Satpam.class);
    }

    private SatpamResponse convertToResponse(Satpam satpam){
        return mapper.map(satpam, SatpamResponse.class);
    }

    @Transactional(isolation = Isolation.READ_UNCOMMITTED)
    @Cacheable(cacheNames = "satpamCache")
    @Override
    public List<SatpamResponse> findAll() {
        simulatedProsess.simulatedProsess();
        var satpamWarga = satpamDao.findAll().stream().filter(satpam -> satpam.getDeleted()
                .equals(Boolean.FALSE)).collect(Collectors.toList());
        return satpamWarga.stream().map(this::convertToResponse).collect(Collectors.toList());
    }

    @CacheEvict(cacheNames = "satpamCache", allEntries = true)
    @Override
    public Map<String, Object> createSatpam(Long alamatId, SatpamRequest satpamRequest) {
        Map<String, Object> result = new HashMap<>();
        simulatedProsess.simulatedProsess();
        if (alamatDao.existsById(alamatId)) {
            var getData = alamatDao.findById(alamatId);
            var setSatpamWarga = convertToEntity(satpamRequest);
            setSatpamWarga.setDeleted(Boolean.FALSE);
            setSatpamWarga.setAlamat(getData.get());
            setSatpamWarga.setNama(satpamRequest.getNama());
            setSatpamWarga.setNoTelepon(satpamRequest.getNoTelepon());
            setSatpamWarga.setShift(satpamRequest.getShift());
            var resultInput = satpamDao.save(setSatpamWarga);

            result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
            result.put(Constant.MESSAGE_STRING, Constant.SUCCESS_STRING);
            result.put(Constant.DATA_STRING, convertToResponse(resultInput));
            return result;
        }
        result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
        result.put(Constant.MESSAGE_STRING, Constant.ID_NOT_FOUND);
        return result;

    }

    @Caching(evict = { @CacheEvict(cacheNames = "satpam", key = "#id"),
            @CacheEvict(cacheNames = "satpamCache", allEntries = true) })
    @Override
    public void delete(Long id) {
        simulatedProsess.simulatedProsess();
        var delete = satpamDao.findById(id).stream().filter(satpam ->
                satpam.getDeleted().equals(Boolean.FALSE)).findFirst();
        if(delete.isPresent()){
            delete.get().setDeleted(Boolean.TRUE);
            satpamDao.save(delete.get());
        }
        delete.orElseThrow();
    }

    @Transactional(propagation = Propagation.REQUIRED)
    @Cacheable(cacheNames = "satpamCache", key = "#id", unless = "#result == null")
    @Override
    public Map<String, Object> getById(Long id) {
        Map<String, Object> result = new HashMap<>();
        simulatedProsess.simulatedProsess();
        try {
            var data = satpamDao.findById(id);
            result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
            result.put(Constant.MESSAGE_STRING, Constant.SUCCESS_STRING);
            result.put(Constant.DATA_STRING, convertToResponse(data.get()));
        } catch (Exception e) {
            result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
            result.put(Constant.MESSAGE_STRING, Constant.ID_NOT_FOUND);
        }
        return result;
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @CacheEvict(cacheNames = "satpamCache", key = "#id", allEntries = true)
    @Override
    public Map<String, Object> updateSatpam(Long id, SatpamRequest satpamRequest) {
        Map<String, Object> result = new HashMap<>();
        simulatedProsess.simulatedProsess();
        try {
            var data = satpamDao.findById(id);
            data.get().setNama(satpamRequest.getNama());
            data.get().setNoTelepon(satpamRequest.getNoTelepon());
            data.get().setShift(satpamRequest.getShift());

            Satpam resultInput = satpamDao.save(data.get());

            result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
            result.put(Constant.MESSAGE_STRING, Constant.SUCCESS_STRING);
            result.put(Constant.DATA_STRING, convertToResponse(resultInput));
        } catch (Exception e) {
            result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
            result.put(Constant.MESSAGE_STRING, Constant.ID_NOT_FOUND);
        }
        return result;
    }


}
