package com.data.warga.data_warga.service;

import com.data.warga.data_warga.dto.PekerjaanRequest;
import com.data.warga.data_warga.dto.PekerjaanResponse;

import java.util.List;
import java.util.Map;

public interface PekerjaanService {
    public List<PekerjaanResponse> findAll();
    public Map<String, Object> create(Long wargaId, PekerjaanRequest request);
    void delete(Long id);
    public Map<String, Object> getById(Long id);
    public Map<String, Object> update(Long id, PekerjaanRequest request);

}
