package com.data.warga.data_warga.models.area;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "Kelurahan")
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class Kelurahan {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "kel_id", nullable = false)
    private Integer kel_id;

    @Column(name = "kel_name", nullable = false, length = 50)
    private String kel_name;

    @ManyToOne(cascade = CascadeType.ALL,targetEntity = Kecamatan.class)
    @JoinColumn(name = "kel_kec_id", referencedColumnName = "kec_id")
    private Kecamatan kel_kec_id;

    @Column(name = "kel_seq", nullable = false)
    private Integer kel_seq;

    @Column(name = "kel_bps_code", nullable = false, length = 10)
    private String kel_bps_code;

    @Column(name = "kel_created_by", nullable = false, length = 30)
    private String kel_created_by;

    @Column(name = "kel_created_date")
    private Date kel_created_date;

    @Column(name = "kel_updated_by", nullable = false, length = 30)
    private String kel_updated_by;

    @Column(name = "kel_updated_date")
    private Date kel_updated_date;

    @Column(name = "kel_zipcode", nullable = false, length = 10)
    private String kel_zipcode;
}
