package com.data.warga.data_warga.models.area;

import com.data.warga.data_warga.models.Warga;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "kota_kabupaten")
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class Kota {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "city_id", nullable = false)
    private Integer id;

    @Column(name = "city_name", nullable = false, length = 50)
    private String cityName;

    @ManyToOne(cascade = CascadeType.ALL,targetEntity = Province.class)
    @JoinColumn(name = "city_prov_id", referencedColumnName = "province_id")
    private Province province;

    @Column(name = "city_seq", nullable = false)
    private Integer citySeq;

    @Column(name = "city_bps_code", nullable = false, length = 5)
    private String cityBpsCode;

    @Column(name = "city_created_by", nullable = false, length = 30)
    private String createdBy;

    @Column(name = "city_created_date")
    private Date createdDate;

    @Column(name = "city_updated_by", nullable = false, length = 30)
    private String updatedBy;

    @Column(name = "city_updated_date")
    private Date updatedDate;
}
