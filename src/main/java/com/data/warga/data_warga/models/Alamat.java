package com.data.warga.data_warga.models;

import com.data.warga.data_warga.models.Entity.BaseEntity;
import com.data.warga.data_warga.models.area.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import static com.data.warga.data_warga.components.constant.AlamatConstant.*;
import static com.data.warga.data_warga.components.constant.AlamatConstant.nullKelurahan;

@Data
@Entity
@Table(name = "Alamat")
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class Alamat extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, length = 20)
    private Long id;

    @Column(name = "Alamat", nullable = false, length = 50)
    private String alamat;

    @ManyToOne(cascade = CascadeType.ALL,targetEntity = Kelurahan.class)
    @JoinColumn(name = "kel_id", referencedColumnName = "kel_id")
    private Kelurahan kel_id;

    @ManyToOne(cascade = CascadeType.ALL,targetEntity = Kecamatan.class)
    @JoinColumn(name = "kec_id", referencedColumnName = "kec_id")
    private Kecamatan kec_id;

    @ManyToOne(cascade = CascadeType.ALL,targetEntity = Kota.class)
    @JoinColumn(name = "city_id", referencedColumnName = "city_id")
    private Kota city_id;

    @ManyToOne(cascade = CascadeType.ALL,targetEntity = Province.class)
    @JoinColumn(name = "province_id", referencedColumnName = "province_id")
    private Province province_id;

    @ManyToOne(cascade = CascadeType.ALL,targetEntity = Zipcode.class)
    @JoinColumn(name = "zipcode", referencedColumnName = "zipcode")
    private Zipcode zipcode;

    @ManyToOne(cascade = CascadeType.ALL,targetEntity = Warga.class)
    @JoinColumn(name = "data_warga_id", referencedColumnName = "id")
    private Warga warga;
}
