package com.data.warga.data_warga.models;

import com.data.warga.data_warga.models.Entity.BaseEntity;
import com.data.warga.data_warga.Enum.DataPengurusEnum;
import com.data.warga.data_warga.Enum.JurusanPendidikan;
import com.data.warga.data_warga.Enum.Pendidikan;
import com.data.warga.data_warga.Enum.Status;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.springframework.format.annotation.DateTimeFormat;


import javax.persistence.*;

@Data
@Entity
@Table(name = "Data_Pengurus")
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class DataPengurus extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID", nullable = false, length = 50)
    private Long id;
    @Column(name = "nama", nullable = false, length = 50)
    private String nama;
    @Column(name = "Jabatan", nullable = false, length = 50)
    @Enumerated(EnumType.STRING)
    private DataPengurusEnum jabatan;
    @Column(name = "Umur", nullable = false, length = 50)
    private String umur;
    @Column(name = "pendidikan", nullable = false, length = 50)
    @Enumerated(EnumType.STRING)
    private Pendidikan pendidikan;
    @Column(name = "jurusan", nullable = false, length = 50)
    @Enumerated(EnumType.STRING)
    private JurusanPendidikan jurusan;

    @Column(name = "massa_kerja", nullable = false, length = 50)
    @DateTimeFormat(pattern = "dd-MM-YYYY")
    private String massaKerja;

    @Column(name = "Status", nullable = false, length = 50)
    @Enumerated(EnumType.STRING)
    private Status status;
}
