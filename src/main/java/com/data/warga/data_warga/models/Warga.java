package com.data.warga.data_warga.models;

import com.data.warga.data_warga.Enum.Hubungan;
import com.data.warga.data_warga.models.Entity.BaseEntity;
import com.data.warga.data_warga.Enum.Agama;
import com.data.warga.data_warga.Enum.JenisKelamin;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;

@Data
@Entity
@Table(name = "data_warga")
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class Warga extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, length = 50)
    private Long id;

    @Column(name = "No_Kartu_Keluarga", nullable = false, length = 20)
    private String noKartuKeluarga;

    @Column(name = "No_Telepon", nullable = false, length = 20)
    private String noTelepon;

    @Column(name = "No_KTP", nullable = false, length = 20)
    private String idCard;

    @Column(name = "Nama", nullable = false, length = 50)
    private String nama;

    @Column(name = "Tempat_Tanggal_Lahir", nullable = false, length = 55)
    private String tempatTanggalLahir;

    @Column(name = "Agama",nullable = false, length = 50)
    @Enumerated(EnumType.STRING)
    private Agama agama;

    @Column(name = "Hubungan",nullable = false, length = 50)
    @Enumerated(EnumType.STRING)
    private Hubungan hubungan;

    @Column(name = "Jenis_Kelamin", nullable = false, length = 30)
    @Enumerated(EnumType.STRING)
    private JenisKelamin jenisKelamin;

}
