package com.data.warga.data_warga.models;

import com.data.warga.data_warga.models.Entity.BaseEntity;
import com.data.warga.data_warga.Enum.Shift;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;

@Data
@Entity
@Table(name = "Satpam")
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class Satpam extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, length = 50)
    private Long id;

    @Column(name = "Nama", nullable = false, length = 50)
    private String nama;

    @Column(name = "no_telpon", nullable = false, length = 50)
    private Integer noTelepon;

    @Column(name = "Shift", nullable = false, length = 50)
    @Enumerated(EnumType.STRING)
    private Shift shift;

    @OneToOne(cascade = CascadeType.ALL,targetEntity = Alamat.class)
    @JoinColumn(name = "alamat_id", referencedColumnName = "id")
    private Alamat alamat;
}
