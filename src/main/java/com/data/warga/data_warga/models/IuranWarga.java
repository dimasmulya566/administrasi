package com.data.warga.data_warga.models;

import com.data.warga.data_warga.models.Entity.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.math.BigDecimal;

@Data
@Entity
@Table(name = "Iuran_Warga")
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class IuranWarga extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, length = 20)
    private Long id;

    @Column(name = "Iuran_Sampah", nullable = false, length = 20)
    private BigDecimal iuranSampah;
    @Column(name = "Iuran_Kebersihan", nullable = false, length = 20)
    private BigDecimal iuranKebersihan;
    @Column(name = "Iuran_Keamanan", nullable = false, length = 20)
    private BigDecimal iuranKeamanan;
    @Column(name = "Iuran_Kas_RT", nullable = false, length = 20)
    private BigDecimal iuranKasRT;
    @Column(name = "Iuran_Kas_RW", nullable = false, length = 20)
    private BigDecimal iuranKasRW;
    @ManyToOne(cascade = CascadeType.ALL,targetEntity = Warga.class)
    @JoinColumn(name = "data_warga_id", referencedColumnName = "id")
    private Warga warga;


}
