package com.data.warga.data_warga.models.area;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "Zipcode")
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class Zipcode {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "zipcode", nullable = false,length = 5)
    private Integer zipcode;

    @Column(name = "zip_desc", nullable = false, length = 100)
    private String zip_desc;

    @ManyToOne(cascade = CascadeType.ALL,targetEntity = Kota.class)
    @JoinColumn(name = "zip_city_id", referencedColumnName = "city_id")
    private Kota zip_city_id;

    @Column(name = "zip_created_by", nullable = false, length = 30)
    private String zip_created_by;

    @Column(name = "zip_created_date")
    private Date zip_created_date;

    @Column(name = "zip_updated_by", nullable = false, length = 30)
    private String zip_updated_by;

    @Column(name = "zip_updated_date")
    private Date zip_updated_date;
}
