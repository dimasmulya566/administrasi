package com.data.warga.data_warga.models.area;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "Kecamatan")
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class Kecamatan {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "kec_id", nullable = false)
    private Integer kec_id;

    @Column(name = "kec_name", nullable = false, length = 50)
    private String kec_name;

    @ManyToOne(cascade = CascadeType.ALL,targetEntity = Kota.class)
    @JoinColumn(name = "kec_city_id", referencedColumnName = "city_id")
    private Kota kec_city_id;

    @Column(name = "kec_seq", nullable = false)
    private Integer kec_seq;

    @Column(name = "kec_bps_code", nullable = false, length = 10)
    private String kec_bps_code;

    @Column(name = "kec_created_by", nullable = false, length = 30)
    private String kec_created_by;

    @Column(name = "kec_created_date")
    private Date kec_created_date;

    @Column(name = "kec_updated_by", nullable = false, length = 30)
    private String kec_updated_by;

    @Column(name = "kec_updated_date")
    private Date kec_updated_date;
}
