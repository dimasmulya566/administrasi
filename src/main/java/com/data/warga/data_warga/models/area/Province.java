package com.data.warga.data_warga.models.area;

import com.data.warga.data_warga.Enum.Agama;
import com.data.warga.data_warga.Enum.Hubungan;
import com.data.warga.data_warga.Enum.JenisKelamin;
import com.data.warga.data_warga.models.Entity.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "provinsi")
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class Province {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "province_id", nullable = false)
    private Integer id;

    @Column(name = "province_name", nullable = false, length = 50)
    private String province_name;

    @Column(name = "province_seq", nullable = false)
    private Integer noSeq;

    @Column(name = "province_bps_code", nullable = false, length = 5)
    private String bpsCode;

    @Column(name = "province_created_by", nullable = false, length = 30)
    private String createdBy;

    @Column(name = "province_created_date")
    private Date createdDate;

    @Column(name = "province_updated_by", nullable = false, length = 30)
    private String updatedBy;

    @Column(name = "province_updated_date")
    private Date updatedDate;

}
