package com.data.warga.data_warga.models;

import com.data.warga.data_warga.models.Entity.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Data
@Entity
@Table(name = "Pekerjaan")
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class Pekerjaan extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "Pekerjaan", nullable = false, length = 50)
    private String pekerjaan;
    @NotNull
    @Column(name = "Penghasilan", nullable = false, length = 20)
    private Double penghasilan;
    @ManyToOne(cascade = CascadeType.ALL,targetEntity = Warga.class)
    @JoinColumn(name = "data_warga_id", referencedColumnName = "id")
    private Warga warga;
}
