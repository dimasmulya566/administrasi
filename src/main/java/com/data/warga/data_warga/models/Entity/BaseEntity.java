package com.data.warga.data_warga.models.Entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.security.Timestamp;
import java.time.LocalDateTime;
import java.util.Date;

@Setter
@Getter
@MappedSuperclass
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@EntityListeners(AuditingEntityListener.class)
public class BaseEntity {

    @Column(name = "created_by")
    @CreatedBy
    private String createdBy;

    @Column(name = "created_on", columnDefinition = "DATE")
    @CreatedDate
    private Date createdOn;

    @Column(name = "last_modified_by")
    @LastModifiedBy
    private String lastModifiedBy;


    @Column(name="is_deleted")
    private Boolean deleted;

    @Column(name = "last_modified_on", columnDefinition = "DATE")
    @LastModifiedDate
    private Date lastModifiedOn;

    @PrePersist
    public void prePersist(){
        this.createdBy = getCreatedBy()!=null?getCreatedBy():"ADMIN";
        this.lastModifiedBy = getLastModifiedBy()!=null?getLastModifiedBy():"ADMIN";
    }
    @PreUpdate
    public void preModified(){
        this.lastModifiedBy = "ADMIN NEW";
    }

}
