package com.data.warga.data_warga.helper;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class SimulateProcess {

    public void simulatedProsess(){
        try {
            Thread.sleep(3000L);
        }catch (InterruptedException e){
            log.info("Connection Time out");
            throw new IllegalStateException(e);
        }
    }
}
