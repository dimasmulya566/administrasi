package com.data.warga.data_warga.Enum;

public enum JenisKelamin {
    LAKI_LAKI,
    PEREMPUAN,
    DAN_LAIN_LAIN
}
