package com.data.warga.data_warga.Enum;

public enum JurusanPendidikan {
    TEKNIK_KOMPUTER_JARINGAN,
    TEKNIK_MESIN,
    DESAIN_GRAFIS,
    ANIMASI,
    AKUNTANSI,
    IPA,
    IPS,
    MADRASAH,
    DAN_LAIN_LAIN
}
