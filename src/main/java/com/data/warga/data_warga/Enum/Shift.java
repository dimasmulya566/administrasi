package com.data.warga.data_warga.Enum;

public enum Shift {
    PAGI,
    SIANG,
    SORE,
    MALAM
}
