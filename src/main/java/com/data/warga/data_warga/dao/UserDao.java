package com.data.warga.data_warga.dao;

import com.data.warga.data_warga.components.exception.EtAuthException;
import com.data.warga.data_warga.models.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.relational.core.sql.In;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserDao extends JpaRepository<Users, Integer> {
    Optional<Users> findByUsername(String username);
    @Query(value = "select * from users where email = :email and password :password"  , nativeQuery = true)
    Users findByEmailAndPassword(@Param("email") String email,@Param("password") String password) throws EtAuthException;
    @Query(value = "select count(*) from users where email = :email"   , nativeQuery = true)
    Integer getCountEmail(String email);

    @Query(value = "select count(*) from users where username = :username"   , nativeQuery = true)
    Integer getCountUsername(String username);

    Integer create(String email,String first_name, String last_name, String password, String username) throws EtAuthException;
}
