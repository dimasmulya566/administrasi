package com.data.warga.data_warga.dao;

import com.data.warga.data_warga.components.exception.EtAuthException;
import com.data.warga.data_warga.models.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.Statement;
import java.util.Optional;

@Repository
public class UserDaoImpl{

    private static final String SQL_CREATE = "INSERT INTO public.users\n" +
            "(id,email, first_name, last_name, \"password\", username)\n" +
            "VALUES(EXTVAL('users_seq'),'', '', '', '', '');\n";

    private static final String SQL_COUNT_EMAIL = "SELECT COUNT(*) FROM public.users WHERE email = ?";
    private static final String SQL_FIND_BY_ID = "SELECT id, username, first_name," +
            " last_name, email, password FROM public.users WHERE id = ?";

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public Optional<Users> findByUsername(String username) {
        return Optional.empty();
    }

    public Users findByEmailAndPassword(String email, String password) throws EtAuthException {
        return null;
    }

    public Integer getCountEmail(String email) {
        return jdbcTemplate.queryForObject(SQL_COUNT_EMAIL, new Object[]{email}, Integer.class);
    }

    public Users findById(Integer id) {
        return jdbcTemplate.queryForObject(SQL_FIND_BY_ID, new Object[]{id},userRowMapper);
    }

    private RowMapper<Users> userRowMapper = ((rs, rowNum) -> {
        return new Users(
                rs.getInt("id"),
                rs.getString("username"),
                rs.getString("first_name"),
                rs.getString("last_name"),
                rs.getString("email"),
                rs.getString("password")

        );
    });

    public Integer create(String email,String first_name, String last_name, String password, String username) throws EtAuthException {
        try {
            KeyHolder keyHolder = new GeneratedKeyHolder();
            jdbcTemplate.update(connection -> {
                var ps = connection.prepareStatement(SQL_CREATE, Statement.RETURN_GENERATED_KEYS );
                ps.setString(1, email);
                ps.setString(2, first_name);
                ps.setString(3, last_name);
                ps.setString(4, password);
                ps.setString(5, username);
                return ps;
            }, keyHolder);
            return (Integer) keyHolder.getKeys().get("id");
        }catch (Exception e){
            throw new EtAuthException("Invalid details. Failed to create account");
        }
    }
}
