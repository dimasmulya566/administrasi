package com.data.warga.data_warga.dao;

import com.data.warga.data_warga.models.Warga;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public interface WargaDao extends PagingAndSortingRepository<Warga, Long>, JpaRepository<Warga,Long> {
    Optional<Warga> findById(Long id);


    @Query(value="SELECT * FROM data_warga dw "
            + "WHERE dw.id = :id "
            + "AND dw.is_deleted = false", nativeQuery= true)
    Warga findByIdTrue(@Param("id") Long id);

    Page<Warga> findByNamaContains(String nama, Pageable pageable);
}
