package com.data.warga.data_warga.dao;

import com.data.warga.data_warga.Enum.ROLE;
import com.data.warga.data_warga.models.Roles;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RolesDao extends JpaRepository<Roles,Long> {
    Optional<Roles> findByName(ROLE name);
}
