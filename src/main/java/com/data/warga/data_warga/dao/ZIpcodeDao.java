package com.data.warga.data_warga.dao;

import com.data.warga.data_warga.models.area.Zipcode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ZIpcodeDao extends JpaRepository<Zipcode,Integer> {
}
