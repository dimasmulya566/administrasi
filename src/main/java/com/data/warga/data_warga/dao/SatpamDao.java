package com.data.warga.data_warga.dao;

import com.data.warga.data_warga.models.Satpam;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SatpamDao extends JpaRepository<Satpam, Long> {
}
