package com.data.warga.data_warga.dao;

import com.data.warga.data_warga.models.Alamat;
import com.data.warga.data_warga.models.Warga;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AlamatDao extends JpaRepository<Alamat, Long> {
    @Query(value="select dw.nama ,a.alamat ,p.province_name ,kk.city_name ,\n" +
            "k.kec_name ,k2.kel_name ,z.zipcode \n" +
            "from alamat a \n" +
            "left join provinsi p ON a.province_id = p.province_id \n" +
            "left join kota_kabupaten kk ON a.city_id = kk.city_id \n" +
            "left join kecamatan k ON a.kec_id = k.kec_id \n" +
            "left join kelurahan k2 on a.kel_id =k2.kel_id \n" +
            "left join zipcode z on a.zipcode = z.zipcode \n" +
            "left join data_warga dw on a.data_warga_id = dw.id  \n" +
            "where p.province_id = :province_id\n" +
            "and kk.city_id = :city_id\n" +
            "and k.kec_id = :kec_id\n" +
            "and k2.kel_id = :kel_id\n" +
            "and z.zipcode = :zipcode\n" +
            "and dw.id  = :id", nativeQuery= true)
    Alamat createAddress(@Param("province_id") Long province_id,@Param("city_id") Long city_id,
                        @Param("kec_id") Long kec_id, @Param("kel_id") Long kel_id,
                        @Param("zipcode") Long zipcode,@Param("id") Long id);

}
