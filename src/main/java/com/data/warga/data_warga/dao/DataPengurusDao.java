package com.data.warga.data_warga.dao;

import com.data.warga.data_warga.models.DataPengurus;
import com.data.warga.data_warga.models.Warga;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface DataPengurusDao extends PagingAndSortingRepository<DataPengurus, Long> {
    Optional<DataPengurus> findById(Long id);

    @Query(value="SELECT * FROM data_pengurus dp "
            + "WHERE dp.id = :id "
            + "AND dp.is_deleted = false", nativeQuery= true)
    Warga findByIdTrue(@Param("id") Long id);

    Page<DataPengurus> findByNamaContains(String nama, Pageable pageable);
}
