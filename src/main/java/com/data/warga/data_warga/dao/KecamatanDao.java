package com.data.warga.data_warga.dao;

import com.data.warga.data_warga.models.area.Kecamatan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface KecamatanDao extends JpaRepository<Kecamatan,Integer> {
}
